package com.log4j;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class randomTest {
    Random random = Mockito.mock(Random.class);
    random cut = new random(random);
    static Arguments[] randomNumberGeneratorTestargs(){
        return new Arguments[]{
              Arguments.arguments(3,3),
              Arguments.arguments(4,4)
        };
    }
    @ParameterizedTest
    @MethodSource("randomNumberGeneratorTestargs")

    void randomNumberGeneratorTest(int arg, int expected){
        Mockito.when(random.nextInt(11)).thenReturn(arg);
        int actual = cut.randomNumberGenerator();
        Assertions.assertEquals(expected, actual);
    }

}