package com.log4j;

import com.log4j.myException;
import com.log4j.random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Loger {
    static final Logger log = LogManager.getLogger("logger");

    private int randomNum;
    private random random;
    private Random ran;

    public int randomNumWithLog() throws myException {
        ran = new Random();
        random = new random(ran);
        randomNum = random.randomNumberGenerator();
        if(randomNum > 5) {
            log.info("Приложение успешно запущено");
            return randomNum;
        } else {
            log.error("Сгенерированное число - " + randomNum);
            throw new myException("Сгенерированное число - " + randomNum);
        }
    }
}
